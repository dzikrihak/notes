# Intro Object Oriented Programming (OOP)
OOP adalah pardigma pemrograman yang mengorganisir kode kedalam objects & class. 
dengan menerapkan OOP membantu kode kita menjadi efisien, modular dan mudah di maintain.

Dalam OOP ada beberapa konsep yang wajib di fahami:

1. **Classes & Objects**
    - [ Class & Object ](https://gitlab.com/dzikrihak/notes/blob/main/tutorial/series/pemrograman/bahasa-python/OOP/classes_objects/classes_and_objects.md)
2. **Encapsulation**
3. **Inherintance**
4. **Abstraction**
5. **Polymorphism**




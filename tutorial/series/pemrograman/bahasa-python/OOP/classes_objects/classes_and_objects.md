## Class & Object
**class** adalah blueprint atau template yang mendefinisikan sifat dan prilaku suat **object**.
sedangkan **object** adalah instance dari **class**

Berikut contoh penerapan **class** :
```python
class Mobil:
    def __init__(self, pabrik, jenis, tahun):
        self.pabrik = pabrik
        self.jenis = jenis
        self.tahun = tahun

    # ini adalah method
    def starter(self):
        print(f" Mobil ini pabrikan dari {self.pabrik} jenis / modelnya {self.jenis} tahun pembuatan {self.tahun}")

```
**class Mobil** adalah sebuah blueprint yang mendefinisikan sifat dari **Mobil**

# Setup SSH keys for multiple github and gitlab account

## Prepare SSH keys:
Add a new key
```
$ ssh-keygen -t rsa_new -C "your_email@youremail.com"
```
Start ssh-agent
```
$ eval `ssh-agent -s`
```
Clear cached ssh key
```
$ ssh-add -D
```
Add key
```
$ ssh-add ~/.ssh/id_rsa_company
$ ssh-add ~/.ssh/id_rsa_personal
...
```
Recheck
```
$ ssh-add -l
```

## Config SSH
Redirect to .ssh folder
```
$ cd ~/.ssh/
```
Create config file if it doesn't exist
```
$ touch config
```
Edit config file
```
$ vi config
```
Add below
```
# Identity for company account
    Host github-company
    HostName github.com
    User git
    IdentityFile ~/.ssh/id_rsa_company
    IdentitiesOnly yes

# Identity for personal account
    Host gitlab-personal
    HostName gitlab.com
    User git
    IdentityFile ~/.ssh/id_rsa_personal
    IdentitiesOnly yes
```
## Clone and update repo
Clone new repo
```
$ git clone git@github-company:xxx/xxx.git
$ git clone git@gitlab-personal:xxx/xxx.git
```
Update exist repo by rename with backup
```
$ cd existing_repo
$ git remote rename origin old-origin
$ git remote add origin git@github-company:xxx/repo_name.git
```
Update exist repo by change url origin
```
$ cd existing_repo
$ git remote set-url origin git@github-company:repo/repo.git
```
